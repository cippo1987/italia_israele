# Incontro IT-IL
Responsabile scientifico [Dr. Stefano Ventura](https://publications.cnr.it/authors/stefano.ventura)
## Bando collaborazione IT-IL
1. 2 aree tematiche
	a. Idrogeno per le rinnovabili
	b. smart material per robotica
2. Partnership tra due universita' una IT una IL per due anni
3. 50K annui per istituto
4. Focus: prodotti, interscambio e mobilita'
5. Sottomissione entro 2 Maggio, modificabile fino al 12 Maggio

Bando nuovo uscito da 2 giorni, non riesco a trovarlo quindi...  
[~~Bando Vecchio~~](https://first.art-er.it/_aster_/viewNews/49690/italia-israele-bando-scientifico-per-progetti-di-ricerca)
[Bando Italiano](https://www.esteri.it/it/diplomazia-culturale-e-diplomazia-scientifica/cooperscientificatecnologica/accordi_coop_indscietec/)

Esiste anche contro-parte industriale:
[Link](https://apre.it/bando-industriale-2022-italia-israele-per-cooperazione-industriale-scientifica-e-tecnologica/).
## Mostra Ricercatrici donne
Si organizzera' la mostra "La scienza e' donna" promossa dalla Fondazione Bracco. Prima meta' di maggio ad Haifa. 
[Evento precedente](https://www.aise.it/rete-diplomatica/una-vita-da-scienziata-in-mostra-a-citt%C3%A0-del-messico-con-ambasciata-e-iic/171858/160)
Dovrebbe svolgersi all'interno della _Giornata della Ricerca Italiana nel Mondo_.


## Progetti
Si sono discusse varie idee per coinvolgere il tessuto economico e quello scientifico.
1. Organizzare un workshop Inizio Settembre 2022, o Ottobre-inizio Novembre 2022.
    a. Workshop
    b. Piccola conferenza
2. Creare una serie di interviste anche video, o comunque contenuti 

## Menzioni
Durante la discussione sono state menzionate attivita', opportunita' e enti vari:
- [Innovitalia](https://innovitalia.esteri.it/)
- Progetti Twinning IT-IL per uniformare i titoli
- [Startup National Central](https://startupnationcentral.org/)
- [Agenzia ICE della camera di commercio](https://www.ice.it/it)


### Note
- Il documento e' scritto in MarkDown, se avete letto la versione raw potete visualizzarla con stackedit.io and dillinger.io
- Se siete pratici di versioning e gitlab, potete unirvi a questa repository e aggiungere/modificare file/pezzi/cose
